import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import axios from 'axios'

Vue.config.productionTip = false
Vue.prototype.$http = axios.create({
  baseURL: process.env.VUE_APP_API_BASE_URL,
  withCredentials: true,
  headers: {
    Authorization: process.env.VUE_APP_API_TOKEN
  }
})

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
